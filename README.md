X proxy manager service:

The proxy manager (proxymngr) is responsible for resolving requests from
xfindproxy (and other similar clients), starting new proxies when
appropriate, and keeping track of all of the available proxy services.
The proxy manager strives to reuse existing proxies whenever possible.

All questions regarding this software should be directed at the
Xorg mailing list:

  https://lists.x.org/mailman/listinfo/xorg

The master development code repository can be found at:

  https://gitlab.freedesktop.org/xorg/app/proxymngr

Please submit bug reports and requests to merge patches there.

For patch submission instructions, see:

  https://www.x.org/wiki/Development/Documentation/SubmittingPatches

